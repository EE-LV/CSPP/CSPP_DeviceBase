CS++ DeviceBase README
======================
The Device Base package is the basis for most of the devices used within CS++. It contains base actor classes which are sometimes a bit larger and more complex with the
advantage that their child-classes should be as easy and small as possible. The overall aim is to have base classes which implement everything from data manipulation and
transfer to user interfaces with the exception of the direct communication with the device driver itself which has to be done within the child classes. Therefore this 
package also includes default GUIs for all base classes.

| Device Base class name | type | package for child class actors |
| ---------------------- | ---- | ------------------------------ |
| CSPP_DCPwr             | DC PowerSupplies         | [CSPP_PowerSupply](https://git.gsi.de/EE-LV/CSPP/CSPP_PowerSupply) |
| CSPP_DMM               | Digital Multi Meters     | - |
| CSPP_FGen              | Function Generators      | [CSPP_FGen](https://git.gsi.de/EE-LV/CSPP/CSPP_FGen) |
| CSPP_MCS               | Multi-Channel Scaler     | [CSPP_FPGA](https://git.gsi.de/EE-LV/CSPP/CSPP_DN/CSPP_FPGA) |
| CSPP_Motor             | Step Motors              | [CSPP_Motion](https://git.gsi.de/EE-LV/CSPP/CSPP_Motion) |
| CSPP_PPG               | Pulsed-Pattern Generator | [CSPP_FPGA](https://git.gsi.de/EE-LV/CSPP/CSPP_DN/CSPP_FPGA) |
| CSPP_Scope             | Oscilloscopes            | [CSPP_Acquisition](https://git.gsi.de/EE-LV/CSPP/CSPP_Acquisition) |

Required CS++ packages
----------------------

1. [**CSPP_Core**](https://git.gsi.de/EE-LV/CSPP/CSPP_Core): The core package is needed for all CS++ applications.

CSPP_DCPwr
----------
This is the base actor for normal DC power supplies as well as DC switches. For this all actors inheriting from this base actor first need to define the channel names 
(and maybe the so-called channel multiplier within the Ini-file):

```
[myDCPwr]
...
CSPP_DCPwr:CSPP_DCPwr.ChannelNames="0,1"
CSPP_DCPwr:CSPP_DCPwr.ChannelMultiplier="L,H"
```

The names of the different channels of a power supply can be set freely here. The channel multiplier list is implemented mainly for switches. Such switches very often have
at least two different Set values per channel depending on the state of the TTL control input of this channel. In all other, non-switch power supplies this entry can be skipped.
An URL section for a power supply could look like this:

```
[myDCPwr.URLs]
...
NumberOfChannels="ni.var.psp://localhost/CSPP_DeviceBase_SV/myDCPwr_NumberOfChannels"
#For all Channels:
#Channel_0:
Get_Voltage_0="ni.var.psp://localhost/CSPP_DeviceBase_SV/myDCPwr_Get_Voltage_0"
Get_Current_0="ni.var.psp://localhost/CSPP_DeviceBase_SV/myDCPwr_Get_Current_0"
Get_OnOff_0="ni.var.psp://localhost/CSPP_DeviceBase_SV/myDCPwr_Get_OnOff_0"
Set_Voltage_0L="ni.var.psp://localhost/CSPP_DeviceBase_SV/myDCPwr_Set_Voltage_0L"
Set_Voltage_0H="ni.var.psp://localhost/CSPP_DeviceBase_SV/myDCPwr_Set_Voltage_0H"
Set_Current_0L="ni.var.psp://localhost/CSPP_DeviceBase_SV/myDCPwr_Set_Current_0L"
Set_Current_0H="ni.var.psp://localhost/CSPP_DeviceBase_SV/myDCPwr_Set_Current_0H"
Set_OnOff_0="ni.var.psp://localhost/CSPP_DeviceBase_SV/myDCPwr_Set_OnOff_0"
```

There are three types of PVs here: 
1. Single variables for complete devices (like *NumberOfChannels*). 
2. Variables needed for each channel (like *Get_Voltage_0). Here one has to use the channel name of the ChannelName list from above as a post-fix to the PV name separated with an underscore (_).
3. Variables needed for each channel and each multiplier (like *Set_Voltage_0L*). Here one has to simple add the name of the channel multiplier to the PV name. If the multiplier list is empty, these variables will be treated like variables from type (2).

The following table shows all variables the DCPwr base actor allows to publish (like with all classes this information can also be found very easy within the *Initialize Attributes Core.vi* of the class):

| PV name | type | description | example | Optional? |
| ------- | ---- | ----------- | ------- | --------- |
| NumberOfChannels | OnePerDevice (1) | Total number of channels configured in the Ini-file | NumberOfChannels=... | FALSE |
| Set/Get_OnOff | OnePerChannel (2) | Output of channel On/Off | Set/Get_OnOff_0=... | TRUE | 
| Get_Voltage | OnePerChannel (2) | Voltage Read-back of channel | Get_Voltage_0=... | TRUE | 
| Get_Current | OnePerChannel (2) | Current Read-back of channel | Get_Current_0=... | TRUE |
| Set_Voltage | OnePerMulti (3) | Voltage Set value of channel + multiplier| Set_Voltage_0L=... | TRUE | 
| Set_Current | OnePerMulti (3) | Current Set value of channel + multiplier| Set_Current_0L=... | TRUE |
| RampUp/DownVoltage | OnePerChannel (2) | RampUp/Down Speed of channel | RampUp/DownVoltage_0=... | TRUE |
| Lower/UpperLimit_Current/Voltage | OnePerChannel (2) | Limits of channel | Lower/UpperLimit_Current/Voltage_0=... | TRUE |

Note that for most of the PVs Get and Set variables exist. The Set variables will always be set if a command is sent to the device, the Get variable should be read back from the device. In some cases
only one variable needs to be present in the system. If in doubt GUIs normally subscribe to Get variables.

Most of the variables are optional. The reason for this is that this base class cannot only be used by voltage sources but also by current sources for example.

### Description of the DCPwr source code

This subsection focuses more on the source code, useful especially for the creation of new child classes. Each public action VI used for the messages uses the well known channeling pattern. So the message executes the
action VI like *SetOnOff.vi*:

![Example of a public action VI](Docs/dcpwr_public_action_vi.png)

Within this VI first a helper VI called *ChannelName2Number.vi* is executed. This VI calculates the channel number (and also the number of the channel multiplier if needed) and can be used in all child classes as well.
Afterwards the dynamic dispatch *Set On/Off Core.vi* is executed and if this was successful the new value is saved in the attribute data and published to a Set-PV. 

These public action VIs are more or less all the same for every function of the power supply. The structure of the core VIs however can vary depending on the specific function and the concrete power supply used.
In principle there are two different ways the PVs of a device could react to a Set command like *Set_Voltage*:

1. The Set-PV should be updated immediately (this is done in all public action VIs!), but the Get-PV should not be touched. The Get-PV will be read-back by executing the Get_XXX (for example *Get_Voltage*) message somewhere else (maybe within a periodic loop).
Within this process the Get-PV is automatically updated.
2. The Set-PV should be updated immediately but the power supply has no read-back for the current status of this variable (or it should not be used). In this case, since the GUI might need the Get-variable, the Get-PV will
automatically also be updated once a Set-PV message was sent.

For many variables from DCPwr option (2) is implemented as default. One can see this in the *Set_XXX Core.vis* of the *CSPP_DCPwr* base class. If a Get-PV can be updated automatically while sending the command to the device 
it will happen here. Note that child classes can decide to use this functionality or not. For example, this functionality is implemented for Set/Get-OnOff. Some power supplies might have boolean read-backs
to indicate whether their output is on or off, and some might not have this. This difference is reflected by if the parent class method is executed in the child classes or not (example from the GSI-HVSwitch2 class):

![SetOnOff Core of the GSI-HVSwitch2](Docs/gsi-hvswitch2_setonoffcore.png)

Here, the last sub-vi is actually the execution of the parent class *Set OnOff Core.vi* in which the Get-PV variable will be set. But this could be also deleted if all GSI-HVSwitches2 should read out this information
in another way.

CSPP_DMM
--------

CSPP_FGen
---------
This is the base actor for all function generators within CS++. The structure of this base class is very similar to the DCPwr base class so most of the descriptions above are also valid here. An example URL section for such a class could look like:

```
[myFGen.URLs]
...
NumberOfChannels="ni.var.psp://localhost/CSPP_DeviceBase_SV/myFGen_NumberOfChannels"
#For all Channels:
#Channel_0:
Get_OnOff_0="ni.var.psp://localhost/CSPP_DeviceBase_SV/myFGen_Get_OnOff_0"
Get_Amplitude_0="ni.var.psp://localhost/CSPP_DeviceBase_SV/myFGen_Get_Amplitude_0"
Get_Frequency_0="ni.var.psp://localhost/CSPP_DeviceBase_SV/myFGen_Get_Frequency_0"
Get_DCOffset_0="ni.var.psp://localhost/CSPP_DeviceBase_SV/myFGen_Get_DCOffset_0"
Get_BurstMode_0="ni.var.psp://localhost/CSPP_DeviceBase_SV/myFGen_Get_BurstMode_0"
Get_Phase_0="ni.var.psp://localhost/CSPP_DeviceBase_SV/myFGen_Get_Phase_0"
Get_BurstCounts_0="ni.var.psp://localhost/CSPP_DeviceBase_SV/myFGen_Get_BurstCounts_0"
Get_WaveformType_0="ni.var.psp://localhost/CSPP_DeviceBase_SV/myFGen_Get_WaveformType_0"
Get_Trigger_0="ni.var.psp://localhost/CSPP_DeviceBase_SV/myFGen_Get_Trigger_0"
```

So again we have for each PV a Set and a Get variant, but since they are identical for a function generator only the Get versions are published (see above).
Unique for the function generator base class is only the WaveformType variable. This is a ring which connects a number to each waveform type:

| FGen base class number | waveform type |
| ---------------------- | ------------- |
| 0 | Sine |
| 1 | DC |
| 2 | Square |
| 3 | Triangle |
| 4 | Ramp |
| 5 | Pulse |
| 6 | Noise |
| 7 | Sync |
| 8 | Negative Ramp |
| 9 | Exponential Rise |
| 10 | Exponential Fall |
| 11 | Cardiac |
| 12 | Arbitrary | 

If a child class should be able to change the waveform type of a channel it might be a good idea to create a conversion VI (see *Agilent33XXX* and *Agilent3352X* classes for example).

CSPP_MCS
--------

CSPP_Motor
----------

CSPP_PPG
--------

CSPP_Scope
----------

Related information
---------------------------------
Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2020  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.